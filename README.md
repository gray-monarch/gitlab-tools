* ### `backup-gitlab.sh`

  1. Create a personal access token with API privileges here: https://gitlab.com/profile/personal_access_tokens
  2. `echo <personal_access_token> > ~/.gitlab`
    
    ```
    Usage:

    ./backup-gitlab.sh -[[p[m|r]]PDdh]

    Purpose: Clones and/or optionally fetches, pulls, or rebases and/or
    prunes all of your gitlab repositories into the _current directory_.
    Below actions are against ALL REPOS.

        [blank]      do a `git clone` (default)
            -p       do a `git pull --ff-only` fast-forward only merge
            -m       do a `git pull --no-ff` merge with a merge commit
            -r       do a `git pull --rebase --autostash`
            -P       do a `git remote prune origin`
            -D       do a `rm -rf` repositories that don't match the api (with confirm)
            -d       print debug headers for repo information
            -h       print this help message

    Note: -m and -r are mutually exclusive options to -p

    Example clone AND/OR fetch ( git clone/fetch only ):

      ./backup-gitlab.sh

    Example clone AND/OR pull ( ff-only merge ):

      ./backup-gitlab.sh -p

    Example clone AND/OR pull AND rebase ( rebase /w autostash ):

      ./backup-gitlab.sh -pr

    Example clone AND/OR pull AND commit merge ( no-ff merge ):

      ./backup-gitlab.sh -pm

    Example clone AND/OR pull AND rebase AND prune

      ./backup-gitlab.sh -prP

    ```
