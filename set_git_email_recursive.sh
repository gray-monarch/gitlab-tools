#!/bin/bash
# Useful for recursively setting `git config user.email`
#
# Author: Michael Goodwin

GIT_REPOS=( $(find "$1" -type d -name '.git' ) )

for ((i=0; i<${#GIT_REPOS[@]}; i++)); do
	GIT_DIR="$( readlink -e "${GIT_REPOS[i]}" )"
	export GIT_DIR
	if [[ $2 ]]; then
		eval git config --local "${2:+user.email \"${2}\"}"
		if [[ $3 ]]; then
			eval git config --local "${3:+user.name \"${3}\"}"
		fi
	fi
	git config --local --get-regexp "user.*"
done






