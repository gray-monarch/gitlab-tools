Useful Git Commands (A modest git cheat sheet)
==========

#### Legend

|         Thing         | Meaning                                                                                       |
|:---------------------:|-----------------------------------------------------------------------------------------------|
|       `<word>`        | User entered variable                                                                         |
|       `[word]`        | Optional command argument                                                                     |
|       **stash**       | "Loosely saves" changes to a special git stash area (i.e. don't need to commit to a "commit") |
|  **staged/unstaged**  | Added / not-added to the index                                                                |
| **indexed/unindexed** | Tracked / un-tracked by the git repo                                                          |
|     `<commitish>`     | Anything that can reference a point in the git repo. A tag, a branch name, a short/long hash  |
|        :poop:         | Use sparingly, can cause bad habits                                                           |
|  :white_check_mark:   | Promotes good practices                                                                       |
|     :exclamation:     | Possibly dangerous, probably re-writes history and may affect collaboration                   |

#### Stashing

|   | Command                          | Description                                                                                           | When you want to...                                                                              |
|---|----------------------------------|-------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
|   | `git stash [save]`               | Stash (loosely save) the current working-directory's changes for index'd (files tracked by git) files | Temporarily save added _AND_ not-added changes to tracked files, but not untracked files         |
|   | `git stash save -k <message>`    | Stash the current working-directory's un-staged changes<br> `-k` means "keep index"                   | Temporarily save not-added changes, but leave files already added to be committed                |
|   | `git stash save -u -k <message>` | Stash the current working-directory's un-index and unstaged changes, but keep the index               | Temporarily save not-added and not-tracked files, but leave files already staged to be committed |
|   | `git stash pop`                  | Apply the top-most change-set as a patch to the current working-directory                             | Restore the last thing that you stashed                                                          |

#### Adding / Removing
|               | Command                                        | Description                              | When you want to...                                      |
|---------------|------------------------------------------------|------------------------------------------|----------------------------------------------------------|
|               | `git add <directory_path>`                     | Recursively add all changes              | Add all changes from a subdir of the repo (including untracked) |
| :exclamation: | `git update-index --[no-]skip-worktree <file>` | Instruct git to ignore changes to a file | Change a config locally and not store that configuration |
|               | <code>git ls-files &#124;</code>`grep -v '^H'` | List all previously flagged files        | Check for `skip-worktree` or `assume-unchanged` files    |
#### Viewing

|                    | Command                                     | Description                                                                                                                                          | When you want to...                                                                                         |
|--------------------|---------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|
|                    | `git show`                                  |                                                                                                                                                      | Show the diff of the last commit                                                                            |
| :white_check_mark: | `git diff`                                  | Shows the differences between the working directory and HEAD, minus added changes                                                                    | See what's different from the last commit that isn't added yet                                              |
| :white_check_mark: | `git diff --cached`                         | Show the differences between the current index and HEAD, minus un-added changes                                                                      | See what would be committed right before you do a `git commit`                                              |
|                    | `git log [<commitish>[..][...]<commitish>]` | Shows metadata of commits in descending order                                                                                                        | Look through the commit messages for something obvious wihtout seeing code                                  |
|                    | `...` and `..`                              | `..` is a diff between the TIPS of both `<commitish>`<br>`...` any commits that aren't the same between branches starting at the point of divergence | `..` - see actual code differences between two branches<br>`...` - see what commits differ between branches |
| :white_check_mark: | `git log -p`                                | Shows the git log in descending commit sequence with the `git show`-style output for each command                                                    | See the last few commit changes                                                                             |
|                    | `git log --oneline --decorate`              | Shows a simplified one-line log with short hash, and commit summary                                                                                  | Do a quicker version of ^                                                                                   |
|                    | `git log --oneline --decorate --graph `     | Shows the above but also with ASCII graphing, the cheapest, quickest git graph. By default it only shows the current branch                          | Get a cheap yet effective git graph                                                                         |

#### Committing

|                     | Command              | Description                                                                                                                                                                              | When you want to...                                                                                                                                |
|---------------------|----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
|                     | `git commit`         | Make a new commit from changes that were added to the index with `git add`                                                                                                               | Save your changes permanently or semi-permanently on a topic or feature branch respectively                                                        |
| :poop:              | `git commit -a`      | Make a new commit from changes that were added to the index _AND_ modified tracked files                                                                                                 | When you are **certain** that what `git status` shows as modified is exactly what you want to commit (see with `git diff` first) **USE CAREFULLY** |
| :poop:              | `git commit -m`      | Make a new commit from changes that were added to the index _AND_ include the commit message from the commandline                                                                        | When you're too lazy or can't be bothered to use a real editor                                                                                     |
| :poop:              | `git commit -am`     | Combining the two above, add all tracked files to be committed and take the commit message from the commandline                                                                          | When you're super lazy and are in a hurry to commit something                                                                                      |
| :poop: :exclamation: | `git commit --amend` | Replaces the current HEAD with a new HEAD based on a new commit<br>**WARNING**: Redoing the commit changes the hash and will require collaborators to rebase if they have already pulled | Any time you go "oops" and need to change something, anything about the last commit                                                                |
