#!/bin/bash
# Useful oneliner to recursively show statuses of git repositories
# (ahead/behind/etc). This is a script that adds the alias to ~/.gitconfig
# It is later used through `git walk` as a child of the git command
#
# Author: Michael Goodwin

git config --global alias.walk "!find -type d -name \"*.git\" | while read -r line; do d=\"\${line%/.git}\"; pushd \"\$d\" &>/dev/null; echo -e \"\n\\033[35m##### \${d^^} #####\\033[0m\n\"; git -c color.status=always status -sbu; echo; git branch --color=always -vv; popd &>/dev/null; done | less -R"
