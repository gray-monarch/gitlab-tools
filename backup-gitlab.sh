#!/bin/bash
# This is a script that uses the gitlab api to download
# all repositories a user has access to into a hierarchical tree
#
# Author: Michael Goodwin

API_KEY="$( grep -v "^$" ~/.gitlab )"
SERVICE="GITLAB.COM"
API_URL="https://${SERVICE,,}/api/v3"
PAGE_LIMIT="999"
#GIT_REPO_TYPE=

# Change below to https_url_to_repo to use https
API_ATTRIB="ssh_url_to_repo"
# Comment out the below if you want regular workdir repos
#GIT_REPO_TYPE='mirror'
CONCURRENT_JOBS=5

popd() { command popd >/dev/null 2>&1; }
pushd() { command pushd "$1" >/dev/null 2>&1; }

trap 'do_exit' EXIT

json_to_url() {
	env python2 <(
	cat <<-EOF
		import json, sys
		a = json.loads(sys.stdin.read())
		print '%s' % '\0'.join([ s['${API_ATTRIB}'] \
			+ '|' + str(s['archived']) for s in a ]) + '\0'
	EOF
	)
}

wait_on_jobs() {
	get_jobnum() {
		readarray -t __a < <(jobs -rp)
		printf '%s' "${#__a[@]}"
	}
	until (( $(get_jobnum) < CONCURRENT_JOBS )); do
		command sleep 0.01
	done
}

delete_repo_select() {
	printf "\n%s\n" "$1 appears to have been $2 on ${SERVICE}, delete?"
	select reply in "Yes" "No"; do
		case $reply in
			Yes)
				rm -rfv "${1:?ERROR}" 2>&1 | grep -v "$1"'/.git/'; break ;;
			No)
				break	;;
		esac
	done
}

do_exit() {
	readarray -t sockets < <( find "${0%/*}" -mindepth 1 -maxdepth 1 -type s )
	for ((i=0; i<${#sockets[@]}; i++)); do
		ssh -S "${sockets[i]}" -O exit - &>/dev/null
	done
	exit
}

# git command debugger
# git() { echo git "$@"; }

array_contains() {
	local val search="$1"
	shift
	for val; do [[ $val == "$search" ]] && return 0; done
	return 1
}

array_dedup() {
	local array="$1"
	array_copy=( $(eval "echo \"\${${array}[@]}\"") )
	shift
	unset array_copy_dedup
	for val in "${array_copy[@]}"; do
		if ! ( eval array_contains "$val" "${array_copy_dedup[@]}" ); then
			array_copy_dedup+=( "$val" )
		fi
	done
	echo "${array_copy_dedup[@]}"
}

print_line() { eval printf '%.s─' "{1..$(tput cols)}"; echo; }

do_help() {
	local t=$'\t'
	local bt='`'
	cat <<-EOF

		Usage:

		$0 -[[p[m|r]]PDdh]

		Purpose: Clones and/or optionally fetches, pulls, or rebases and/or
		prunes all of your gitlab repositories into the _current directory_.
		Below actions are against ALL REPOS.

	    [blank] ${t} do a ${bt}git clone${bt} (default)
		${t}-p ${t} do a ${bt}git pull --ff-only${bt} fast-forward only merge
		${t}-m ${t} do a ${bt}git pull --no-ff${bt} merge with a merge commit
		${t}-r ${t} do a ${bt}git pull --rebase --autostash${bt}
		${t}-P ${t} do a ${bt}git remote prune origin${bt}
		${t}-D ${t} do a ${bt}rm -rf${bt} repositories that don't match the api (with confirm)
		${t}-d ${t} print debug headers for repo information
		${t}-h ${t} print this help message

		Note: -m and -r are mutually exclusive options to -p

		Example clone AND/OR fetch ( git clone/fetch only ):

		  $0

		Example clone AND/OR pull ( ff-only merge ):

		  $0 -p

		Example clone AND/OR pull AND rebase ( rebase /w autostash ):

		  $0 -pr

		Example clone AND/OR pull AND commit merge ( no-ff merge ):

		  $0 -pm

		Example clone AND/OR pull AND rebase AND prune

		  $0 -prP

	EOF
}

while getopts "pmrPDdh" opt; do
	case "$opt" in
		p) do_pull=1; merge_style=( "--no-ff" ) 							;;
		m) [[ $do_pull ]] \
			&& merge_style=( "--ff-only" )									;;
		r) [[ $do_pull ]] \
			&& do_rebase=1; merge_style=( "--rebase" "--autostash" )		;;
		P) do_prune=1														;;
		D) do_delete=1 														;;
		d) do_debug=1 														;;
	  h|?) do_help; exit	 												;;
	esac
done

#API_RETURN_JSON=$(<json_test_file)
API_RETURN_JSON="$(
	curl -s -H "PRIVATE-TOKEN: ${API_KEY}" \
	"${API_URL}/projects?per_page=${PAGE_LIMIT}"
)"

if [[ ${API_RETURN_JSON} =~ \"message\":\"40[0-9]\ [a-Z] ]]; then
	printf "%s\n" "There's something wrong with your Access Token:" \
	  "${API_RETURN_JSON}"
	exit 1
fi

# Analyze all api paths/names
while IFS='|' read -r -d '' repo archived _; do
	repo_group="$( awk -F'[:/]' '{ print $2 }' <<< "$repo" )"
	repo_url_arr+=( "$repo" )
	repo_dir_arr+=( "${repo##*/}" )
	full_repo_path_arr+=( "${repo##*:}" )
	group_arr+=( "$repo_group" )
	archived_arr+=( "${archived,,}" )
done < <( json_to_url <<< "${API_RETURN_JSON}" )

# Remove .git ending for non-bare repos
if [[ ! $GIT_REPO_TYPE =~ (mirror|bare) ]]; then
		repo_dir_arr=( "${repo_dir_arr[@]%.git}" )
		full_repo_path_arr=( "${full_repo_path_arr[@]%.git}" )
fi

sub_group_path_arr=( "${full_repo_path_arr[@]#*/}" )
sub_group_path_dir_arr=( "${sub_group_path_arr[@]%/*}" )
# shellcheck disable=SC2034
group_with_sub_dir_arr=( "${full_repo_path_arr[@]%/*}" )

# Read in all local paths
readarray -t local_dir_arr < <( find . -mindepth 2 -maxdepth 2 -type d ! -path "*.git/*" )
local_dir_arr=( "${local_dir_arr[@]#./}" )

# Setup SSH Multiplexing sockets
declare -A ssh_socket_paths
readarray -t ssh_endpoints < <( printf "%s\n" "${repo_url_arr[@]%%:*}" | sort -u )
for ((i=0; i<${#ssh_endpoints[@]}; i++)); do
	current_socket="$HOME/.ssh/${ssh_endpoints[i]}"
	ssh_socket_paths[${ssh_endpoints[i]}]="${current_socket}"
	# shellcheck disable=SC2029
	ssh -N \
		-o "ControlPath=\"${current_socket}\"" \
		-o 'ControlPersist=0' \
		-o 'ControlMaster=auto' \
		"${ssh_endpoints[i]}" || \
		{
			# shellcheck disable=SC2016
			printf '%s\n' \
			'Cannot establish control socket for '"${ssh_endpoints[i]}"'!' \
			'Is your ssh-agent loaded/fowarded properly? Output of `ssh-add -l`:'
			ssh-add -l
			exit 1
		}
done

# Loop on all repo URLs and clone or fetch, or ask delete if archived
[[ $do_debug ]] && print_line
for ((i=0; i<${#repo_url_arr[@]}; i++)); do
	# Don't start another job until the queue is ready
	wait_on_jobs

	repo_url="${repo_url_arr[i]}"
	repo_path="${full_repo_path_arr[i]}"
	repo_dir="${repo_dir_arr[i]}"
	sub_group_path="${sub_group_path_arr[i]}"
	sub_group_dir="${sub_group_path_dir_arr[i]}"
	group_dir="${group_arr[i]}"
	is_archived="${archived_arr[i]}"

	if [[ $do_debug ]]; then
		printf "%-17b%s\n" \
			"Repo URL:" "${repo_url}" \
		 	"Repo Path:" "${repo_path}" \
			"SubGroup Path:" "${sub_group_path}" \
			"SubGroup Dir:" "${sub_group_dir}" \
			"Repo Dir:" "${repo_dir}" \
			"Group:" "${group_dir}" \
		 	"Archived:" "${is_archived}"
		print_line
		continue
	fi

	# Export the right SSH socket path and enable multiplexing
  	export GIT_SSH_COMMAND="ssh \
		-o 'ControlMaster=no' \
		-o 'ControlPath=${ssh_socket_paths[${repo_url%%:*}]}'"

	mkdir -p "${group_dir}"
	if [[ ${sub_group_dir} != "${repo_dir}" ]]; then
		mkdir -p "${group_dir}/${sub_group_dir}"
	fi

	pushd "$group_dir"
	if [[ -d ${sub_group_path} && ${is_archived} == "true" ]]; then
		delete_repo_select "${sub_group_path}" "archived"
	elif [[ ! -d ${sub_group_path} && ${is_archived} == "false" ]]; then
		(
			if [[ ${sub_group_dir} != "${repo_dir}" ]]; then
				pushd "${sub_group_dir}"; sub_pushed=1
			fi
			echo "Cloning ${repo_url}"
			git clone ${GIT_REPO_TYPE:+--${GIT_REPO_TYPE}} \
			"${repo_url}" >/dev/null 2>&1
			(( sub_pushed )) && { popd; unset sub_pushed; }
		) &
	elif [[ $is_archived == false ]]; then
		pushd "${sub_group_path}"
		(
			if [[ $do_pull ]]; then
				if [[ $do_rebase ]]; then
					echo "Pulling, stashing, and rebasing ${repo_url}..."
				else
					echo "Pulling ${repo_url}..."
				fi
				git pull "${merge_style[@]}"
			else
				echo "Fetching ${repo_url}..."
				git fetch --all >/dev/null 2>&1
			fi
			if [[ $do_prune ]]; then
				echo "Pruning ${repo_url}.."
				git remote prune origin
			fi
		) &
		popd # sub_group_path
	fi
	popd # group_dir
done

if (( $(jobs -rp | wc -l) > 0 )); then
	 echo "Waiting on background git operations..."
	 wait
fi

if [[ $do_delete ]]; then
	echo "Processing deletion candidates..."
	# Loop on local dirs in groups that we know of and ask to delete
	for ((i=0; i<${#local_dir_arr[@]}; i++)); do
		local_dir_group="${local_dir_arr[i]%%/*}"
		local_dir_path="${local_dir_arr[i]}"
		group_arr_dedup=( $(array_dedup group_arr) )
		group_with_sub_dir_arr_dedup=( $(array_dedup group_with_sub_dir_arr) )

		if [[ $do_debug ]]; then
			typeset -p local_dir_group local_dir_path group_arr_dedup
			#shellcheck disable=SC2030
			typeset -p group_with_sub_dir_arr_dedup | tr ' ' '\n'
		else
			if (array_contains "${local_dir_group}" "${group_arr_dedup[@]}") \
			&& ! (
					#shellcheck disable=SC2031
					array_contains "${local_dir_path}" \
						"${full_repo_path_arr[@]}" \
						"${group_with_sub_dir_arr_dedup[@]}"
				)
			then
				delete_repo_select "${local_dir_path}" "deleted"
			fi
		fi
	done
fi

echo "Done."
